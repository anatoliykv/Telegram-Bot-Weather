#!/usr/local/bin/python3
import requests
from bs4 import BeautifulSoup as BS
import os
from flask import Flask, request
import telebot
from telebot import types
from geopy.geocoders import Nominatim

geolocator = Nominatim(user_agent="geoapiExercises")
TOKEN = os.environ.get('TOKEN')
bot = telebot.TeleBot(TOKEN)
town = str
server = Flask(__name__)


try:
    @bot.message_handler(commands=["start"])
    def geo(message):
        keyboard = types.ReplyKeyboardMarkup(row_width=1, resize_keyboard=True)
        button_geo = types.KeyboardButton(text="Узнать погоду в Вашем городе", request_location=True)
        keyboard.add(button_geo)
        bot.send_message(message.chat.id, "Привет! Нажми на кнопку и передай мне свое местоположение",
                         reply_markup=keyboard)


    @bot.message_handler(func=lambda message: True, content_types=["location"])
    def start(message):
        global time, cur_city, t_min, t_max, voshod_zakat, text, text2
        if message.location is not None:
            # bot = telebot.TeleBot(TOKEN)
            location = geolocator.reverse("%s, %s" % (message.location.latitude, message.location.longitude))
            address = location.raw['address']
            town = address.get('city', '') or address.get('town', '') or address.get('village', '')
            r = requests.get("https://sinoptik.ua/погода-%s" % town)
            html = BS(r.content, 'html.parser')

            for city in html.select('#header'):
                cur_city = city.select('.cityNameShort')[0].text

            for el in html.select('#content'):
                time = el.select('.today-time')[0].text
                t_min = el.select('.temperature .min')[0].text
                t_max = el.select('.temperature .max')[0].text
                text = el.select('.wDescription .description')[0].text
                text2 = el.select('.oDescription .description')[0].text
                voshod_zakat = el.select('.lSide .infoDaylight')[0].text

            result = (time + '\n\n' + cur_city + '\n\n' + t_min + ' ' +
                      t_max + '\n\n' + voshod_zakat + '\n\n' + text + '\n\n' + text2)
            print(result)
            bot.send_message(message.chat.id, str(result))
            # bot.send_message(message.chat_id, address)
        else:
            bot.send_message(message.chat_id, "Ошибка")


    @bot.message_handler(func=lambda message: True, content_types=['text'])
    def echo_message(message):
        bot.reply_to(message, message.text)


    @server.route('/' + TOKEN, methods=['POST'])
    def getMessage():
        json_string = request.get_data().decode('utf-8')
        update = telebot.types.Update.de_json(json_string)
        bot.process_new_updates([update])
        return "!", 200


    @server.route("/")
    def webhook():
        bot.remove_webhook()
        bot.set_webhook(url='https://waether-anatoliykv.herokuapp.com/' + TOKEN)
        return "!", 200
except Exception as e:
    print(e)

if __name__ == "__main__":
    server.run(host="0.0.0.0", port=int(os.environ.get('PORT', 5002)))
