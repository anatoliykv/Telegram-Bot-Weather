# Telegram-Bot-Weather

[![pipeline status](https://gitlab.com/anatoliykv/Telegram-Bot-Weather/badges/master/pipeline.svg)](https://gitlab.com/anatoliykv/Telegram-Bot-Weather/-/commits/master)

![python](https://wiki.python.org/wiki/europython/img/python-logo.gif)

[Weather Telegram Bot GitLab Pages](https://anatoliykv.gitlab.io/Telegram-Bot-Weather)


# Telegram Bot Weather

A Telegram bot that provides weather information to users. This bot fetches real-time weather data and delivers it directly to the user's Telegram chat. The project leverages public APIs to retrieve weather information based on user input, such as city names or geographic coordinates.

## Table of Contents

- [Introduction](#introduction)
- [Features](#features)
- [Installation](#installation)
- [Usage](#usage)
- [Configuration](#configuration)
- [Bot Commands](#bot-commands)
- [Project Structure](#project-structure)
- [Contributing](#contributing)
- [License](#license)
- [Acknowledgements](#acknowledgements)

## Introduction

The Telegram Bot Weather project is designed to provide users with up-to-date weather information via a simple Telegram bot interface. Users can request weather details by sending commands or messages to the bot, which then responds with current weather conditions, forecasts, and more.

## Features

- **Real-Time Weather Data**: Fetches and delivers current weather information using public weather APIs.
- **Location-Based Queries**: Supports queries by city name or geographic coordinates.
- **User-Friendly Commands**: Easy-to-use commands for interacting with the bot.
- **Multilingual Support**: Provides weather information in multiple languages (if supported by the weather API).
- **Error Handling**: Graceful handling of errors and invalid inputs with informative messages to the user.

## Installation

Follow these steps to set up the Telegram Bot Weather on your local machine or server:

1. **Clone the Repository:**

    ```bash
    git clone https://gitlab.com/anatoliykv/Telegram-Bot-Weather.git
    cd Telegram-Bot-Weather
    ```

2. **Install Dependencies:**

    Ensure you have Python installed. Then, install the required Python packages:

    ```bash
    pip install -r requirements.txt
    ```

3. **Set Up a Telegram Bot:**

    - Create a new bot on Telegram by talking to [BotFather](https://core.telegram.org/bots#botfather).
    - Obtain the API token from BotFather after creating the bot.

4. **Configure Environment Variables:**

    Create a `.env` file in the project root and add your configuration settings:

    ```plaintext
    TELEGRAM_TOKEN=your-telegram-bot-token
    WEATHER_API_KEY=your-weather-api-key
    ```

    Replace `your-telegram-bot-token` with the token from BotFather and `your-weather-api-key` with your weather API key (e.g., from OpenWeatherMap).

5. **Run the Bot:**

    Start the bot by running the main script:

    ```bash
    python bot.py
    ```

## Usage

Once the bot is running, you can interact with it through Telegram. Simply send commands or messages to the bot, and it will respond with weather information.

## Configuration

You can configure the following settings in the `.env` file:

- **TELEGRAM_TOKEN**: The API token for your Telegram bot.
- **WEATHER_API_KEY**: The API key for accessing weather data from a public API (e.g., OpenWeatherMap).
- **DEFAULT_LANGUAGE**: (Optional) Set a default language for the bot responses.
- **DEFAULT_CITY**: (Optional) Set a default city for weather queries if the user doesn't specify one.

## Bot Commands

Here are some of the commands supported by the bot:

- `/start` - Start interacting with the bot.
- `/weather [city]` - Get the current weather for the specified city.
- `/forecast [city]` - Get the weather forecast for the specified city.
- `/help` - Display help information and available commands.

## Project Structure

An overview of the project's directory structure:

