FROM python:3.11.4-alpine AS app
LABEL maintainer="anatoliykv"
LABEL stage="build"
ENV TOKEN=$TOKEN
WORKDIR /app
RUN pip3 install pyTelegramBotApi beautifulsoup4 gunicorn flask geopy nominatim
COPY . .
CMD [ "python3", "app.py" ]
EXPOSE 5000





# FROM python:3.8.3-alpine AS app
# LABEL maintainer="anatoliykv"
# LABEL stage="build"
# WORKDIR /app/
# COPY . /app/
# RUN pip3 install pyTelegramBotApi beautifulsoup4 && \
#     python3 main.py > index.html

# FROM nginx:1.19-alpine AS web
# LABEL stage="deploy"
# WORKDIR /usr/share/nginx/html/
# COPY --from=app /app/index.html .
# EXPOSE 80 443